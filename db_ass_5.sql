-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 05. Nov, 2017 21:47 PM
-- Server-versjon: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ass_5`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `city`
--

CREATE TABLE `city` (
  `name` varchar(64) COLLATE utf8_danish_ci NOT NULL,
  `county` varchar(64) COLLATE utf8_danish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `club`
--

CREATE TABLE `club` (
  `id` varchar(200) COLLATE utf8_danish_ci NOT NULL,
  `Name` varchar(200) COLLATE utf8_danish_ci NOT NULL,
  `city` varchar(64) COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `seasonrecord`
--

CREATE TABLE `seasonrecord` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `club` varchar(200) COLLATE utf8_danish_ci DEFAULT NULL,
  `username` varchar(50) COLLATE utf8_danish_ci NOT NULL,
  `distance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skier`
--

CREATE TABLE `skier` (
  `username` varchar(50) COLLATE utf8_danish_ci NOT NULL,
  `first_name` varchar(45) COLLATE utf8_danish_ci NOT NULL,
  `last_name` varchar(45) COLLATE utf8_danish_ci NOT NULL,
  `YearOfBirth` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Club_City1_idx` (`city`);

--
-- Indexes for table `seasonrecord`
--
ALTER TABLE `seasonrecord`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_SkierSeasonRecord_Club_idx` (`club`),
  ADD KEY `fk_SkierSeasonRecord_Skier1_idx` (`username`);

--
-- Indexes for table `skier`
--
ALTER TABLE `skier`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `seasonrecord`
--
ALTER TABLE `seasonrecord`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1456;
--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `club`
--
ALTER TABLE `club`
  ADD CONSTRAINT `fk_Club_City1` FOREIGN KEY (`city`) REFERENCES `city` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Begrensninger for tabell `seasonrecord`
--
ALTER TABLE `seasonrecord`
  ADD CONSTRAINT `fk_SkierSeasonRecord_Club` FOREIGN KEY (`club`) REFERENCES `club` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_SkierSeasonRecord_Skier1` FOREIGN KEY (`username`) REFERENCES `skier` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
