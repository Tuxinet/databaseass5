<?php

include_once("model/Model.php");
include_once("view/View.php");

class Controller {
    public $model;
    public static $skierDocument;

    public function __construct()
    {
        Controller::$skierDocument = new DOMDocument();
        if (Controller::$skierDocument->load('SkierLogs.xml'))
            print('Loaded XML Document<br>');

        $this->model = new Model();
    }

    public function invoke()
    {
        $view = new View();
        $view->getPageContent();

        $this->model->LoadData();
    }
}

?>