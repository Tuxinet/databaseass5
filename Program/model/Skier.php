<?php

class Skier
{
    public $username;
    public $first_name;
    public $last_name;
    public $YearOfBirth;

    public function __construct($username, $first_name, $last_name, $YearOfBirth)  
    {  
        $this->username = $username;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->YearOfBirth = $YearOfBirth;
    } 
}

?>