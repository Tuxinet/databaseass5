<?php

include_once("controller/Controller.php");
include_once("model/City.php");
include_once("model/Club.php");
include_once("model/SeasonRecord.php");
include_once("model/Skier.php");

class Model
{
    private $xpath;
    private $db = null;
    private $skiers;

    public function __construct($db = null)
    {
        if ($db)
        {
            $this->db = $db;
        }
        else
        {
            // Create PDO connection
            $this->db = new PDO('mysql:dbname=DB_ASS_5' . ';host=localhost',
            "root", "",
            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

            print("Created DB connection<br>");
        }

        $this->xpath = new DOMXpath(Controller::$skierDocument);
        $this->skiers = array();
    }

    public function LoadData()
    {
        $this->ClearData();

        $this->LoadCities();
        $this->LoadClubs();
        $this->LoadSkiers();
        $this->LoadRecords();
    }

    private function ClearData()
    {
        $stmt = $this->db->prepare('DELETE FROM seasonrecord;DELETE FROM club;DELETE FROM city;DELETE FROM skier;');
        $stmt->execute();

    }

    private function LoadCities()
    {
        $citiesList = $this->xpath->query('//City');

        $cities = array();

        foreach($citiesList as $city)
        {
            $county = $this->xpath->query('//Club[City="' . $city->nodeValue . '"]/County')->item(0)->nodeValue;

            $cityObj = new City($city->nodeValue, $county);

            array_push($cities, $cityObj);
        }


        foreach($cities as $city)
        {
            print("Adding city " . $city->name . " in county " . $city->county . "<br>");

            $stmt = $this->db->prepare('INSERT INTO city(name, county) VALUES(:name, :county)');
            $stmt->bindValue(':name', $city->name);
            $stmt->bindValue(':county', $city->county);
            $stmt->execute();
        }
    }

    private function LoadClubs()
    {
        $clubsList = $this->xpath->query('//Club');
        
        $clubs = array();

        foreach($clubsList as $club)
        {
            $id = $club->getAttribute("id");
            $name = $club->getElementsByTagName("Name")->item(0)->nodeValue;
            $city = $club->getElementsByTagName("City")->item(0)->nodeValue;

            $clubObj = new Club($id, $name, $city);
            array_push($clubs, $clubObj);
        }


        foreach($clubs as $club)
        {
            print("Adding club " . $club->name . "<br>");

            $stmt = $this->db->prepare('INSERT INTO club(id, name, city) VALUES(:id, :name, :city)');
            $stmt->bindValue(':id', $club->id);
            $stmt->bindValue(':name', $club->name);
            $stmt->bindValue(':city', $club->city);
            $stmt->execute();
        }
}

    private function LoadSkiers()
    {
        $skierList = $this->xpath->query('//SkierLogs/Skiers/Skier');

        foreach($skierList as $skier)
        {
            $username = $skier->getAttribute("userName");
            $first_name = $skier->getElementsByTagName("FirstName")->item(0)->nodeValue;
            $last_name = $skier->getElementsByTagName("LastName")->item(0)->nodeValue;
            $YearOfBirth = $skier->getElementsByTagName("YearOfBirth")->item(0)->nodeValue;

            $skierObj = new Skier($username, $first_name, $last_name, $YearOfBirth);
            array_push($this->skiers, $skierObj);
        }


        foreach($this->skiers as $skier)
        {
            print("Adding skier " . $skier->username . "<br>");

            $stmt = $this->db->prepare('INSERT INTO skier(username, first_name, last_name, yearofbirth) VALUES(:username, :first_name, :last_name, :yearofbirth)');
            $stmt->bindValue(':username', $skier->username);
            $stmt->bindValue(':first_name', $skier->first_name);
            $stmt->bindValue(':last_name', $skier->last_name);
            $stmt->bindValue(':yearofbirth', $skier->YearOfBirth);
            $stmt->execute();
        }
    }

    private function LoadRecords()
    {
        $seasonList = $this->xpath->query('//Season');

        $seasonRecords = array();
        
        foreach($seasonList as $season)
        {
            $year = $season->getAttribute("fallYear");

            foreach($season->getElementsByTagName("Skiers") as $skierList)
            {
                $clubId = $skierList->getAttribute("clubId");

                foreach($skierList->getElementsByTagName("Skier") as $skier)
                {
                    $username = $skier->getAttribute("userName");
                    $distances = $skier->getElementsByTagName("Distance");
                    $totalDistance = 0;

                    foreach($distances as $distance)
                    {
                        $totalDistance += (int)$distance->nodeValue;
                    }
                    
                    $seasonRecord = new SeasonRecord($year, $clubId, $username, $totalDistance);

                    array_push($seasonRecords, $seasonRecord);
                }
            }
        }


        foreach($seasonRecords as $record)
        {
            print("Adding seasonrecord for " . $record->username . "<br>");

            if ($record->club == null) $record->club = null; // Why are we learning php again?

            $stmt = $this->db->prepare('INSERT INTO seasonrecord(year, club, username, distance) VALUES(:year, :club, :username, :distance)');
            $stmt->bindValue(':year', $record->year);
            $stmt->bindValue(':club', $record->club, PDO::PARAM_NULL);
            $stmt->bindValue(':username', $record->username);
            $stmt->bindValue(':distance', $record->distance);
            $stmt->execute();
        }
    }
}

?>