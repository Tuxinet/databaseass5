<?php

class SeasonRecord
{
    public $year;
    public $club;
    public $username;
    public $distance;

    public function __construct($year, $club, $username, $distance)  
    {  
        $this->year = $year;
        $this->club = $club;
        $this->username = $username;
        $this->distance = $distance;
    } 
}

?>