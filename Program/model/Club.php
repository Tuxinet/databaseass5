<?php

class Club
{
    public $id;
    public $name;
    public $city;

    public function __construct($id, $name, $city)  
    {  
        $this->id = $id;
        $this->name = $name;
        $this->city = $city;
    } 
}

?>