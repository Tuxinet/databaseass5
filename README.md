This is the document where I have all the steps needed to complete all the tasks
in database assignment 5


1.1
$x("//SkierLogs/Season/@fallYear")
List of 2 attribute nodes

1.2
$x('//SkierLogs/Season/Skiers/Skier[@userName="mari_dahl"]/Log/Entry')
59 entries

1.3
$x('//SkierLogs/Season[@fallYear="2015"]/Skiers[@clubId="vindil"]/Skier/Log[Entry[number(Distance) > 10]]')
5 logs

1.4
$x('//SkierLogs/Season[@fallYear="2016"]/Skiers/Skier/@userName[../Log/Entry[contains(Area, "Venabygd")]])
17 usernames

1.5
$x('count(//SkierLogs/Skiers/Skier[YearOfBirth <= 2004 and YearOfBirth >= 2002])')
int 42

1.6
$x('//Season[@fallYear="2016"]//Date[ancestor::Skier[@userName="idar_kals1"] and ancestor::Entry[Area="Lygna"]]')
3 dates

1.7
$x('sum(//Season[@fallYear="2016"]//Distance)')
int 17791

1.8
$x('sum(//Season[@fallYear="2015"]/Skiers[not(@clubId)]//Distance)')
int 587

1.9
$x('//Season[@fallYear="2015"]//Skiers[@clubId=(//Club[County="Oppland"]/@id)]/Skier')
34 skiers

1.10
root = $x("//SkierLogs/..")[0];
$x('SkierLogs/Season/Skiers/Skier[not(@userName =(//Season[@fallYear=2016]/Skiers/Skier[.//Area[contains(text(), "Nordseter")]]/@userName))][.//Area[contains (text(), "Nordseter")]]', root)
5 skiers
